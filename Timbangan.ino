/*
 * Program Arduino - Timbangan Digital
 * Mikrokontroler --> Arduino UNO
 * LCD --> 16 x 2 
 * HX711
 * Load Cell
 */

#include "HX711.h" //memasukan library HX711
#include <LiquidCrystal.h> // Pustaka LCD
LiquidCrystal lcd(13, 12, 11, 10, 9, 8); // Pin LCD --> rs, en, d4, d5, d6, d7

#define DOUT  3 //mendefinisikan pin arduino yang terhubung dengan pin DT module HX711
#define CLK  2 //mendefinisikan pin arduino yang terhubung dengan pin SCK module HX711

HX711 scale;

float calibration_factor = 7050; //nilai kalibrasi awal

void setup() {
  lcd.begin(16, 2);
  scale.begin(DOUT, CLK);
  Serial.begin(9600);
  lcd.print("MemulaiKalibrasi");
  lcd.setCursor(0,1);
  lcd.print("Kosongkan  Beban");
  delay(5000);
  lcd.clear();
  scale.set_scale();
  scale.tare(); // auto zero / mengenolkan pembacaan berat

  long zero_factor = scale.read_average(); //membaca nilai output sensor saat tidak ada beban
  lcd.print("Zero factor: ");
  lcd.setCursor(0,1);
  lcd.print(zero_factor);
}

void loop() {
  scale.set_scale(calibration_factor); //sesuaikan hasil pembacaan dengan nilai kalibrasi
  lcd.setCursor(0,0);
  lcd.print("Berat: ");
  lcd.print(scale.get_units(), 1);
  lcd.print(" kg");
  lcd.setCursor(0,1);
  lcd.print(" calibration_factor: ");
  lcd.print(calibration_factor);

  //Untuk Kalibrasi
  if(Serial.available())
  {
    char temp = Serial.read();
    if(temp == '+' || temp == 'a')
      calibration_factor += 10;
    else if(temp == '-' || temp == 'z')
      calibration_factor -= 10;
  }
}